package com.task54_10;

//import com.devcamp.task54.CCar;
//import com.devcamp.task54.CMotorbike;
//import com.devcamp.task54.CVehicle;

public class MainCar {
	public static void main(String[] args) {
//		CVehicle myFastCar = new CCar("Volvo",2021,"SC40","acb1");
//		myFastCar.honk();
//		myFastCar.print();
//		System.out.println("Max speed: " + String.valueOf(myFastCar.maxSpeedPerKm()));
//		//System.out.println(myFastCar.brand + " " + myFastCar.modelName);
//		System.out.println("---------------");
//		CMotorbike myMotorbike = new CMotorbike();
//		myMotorbike.getMaxSpeedPerKm(500);
//		myMotorbike.print("myMotorbike");
		CVehicle car = new CCar("VIN",2020,"VIP","id01");
		car.print();
		System.out.println("Max speed: " + String.valueOf(car.maxSpeedPerKm()));
		System.out.println("--------------------------------");
		CCar myFastCCar = new CCar();
		myFastCCar.honk();
		//System.out.println(myFastCCar.brand + " " + myFastCCar.getModelName());
		System.out.println("Max speed: " + String.valueOf(myFastCCar.maxSpeedPerKm()));
		System.out.println("--------------------------------");
		//CMotorbike motorbike = new CMotorbike();
		car = new CMotorbike();
		car.brand = "HONda";
		car.yearManufactured = 2019;
		car.print("CMotorbike123");
		System.out.println("Max speed: " + String.valueOf(car.maxSpeedPerKm()));
		System.out.println("--------------------------------");
	}
}
