package com.task54_10;

class CCar extends CVehicle {
	private String modelName;
	private String vid;

	//
	public CCar() {
	}

	//
	public CCar(String paramBrand, int paramYearManufactured, String paramModelName, String paramVid) {
		super(paramBrand, paramYearManufactured);
		modelName = paramModelName;
		vid = paramVid;
	}

}
