package com.task54_10;

class CVehicle {
	  // can use at (subclass)
	  protected String brand;
	  protected int yearManufactured;
	  protected int maxSpeed;
	  // 
	  public CVehicle() {
	  }  
	  // 
	  public CVehicle(String paramBrand, int paramYearManufactured) {
		brand = paramBrand;
	    yearManufactured = paramYearManufactured; 
	  }
	  // max 
	  public void print() {
	    System.out.println("CVehicle information... ");
	  	System.out.println("brand: " + brand);
	    System.out.println("year manufactured: " + yearManufactured);
	  }
	  public void print(String Class) {
		    System.out.println(Class + " information... ");
		  	System.out.println("brand: " + brand);
		    System.out.println("year manufactured: " + yearManufactured);
		    System.out.println("Max Speed: " + this.maxSpeed);
		  }
	  //max speed of vehicle
	  public int maxSpeedPerKm() {
		return 200; 
	  };
	  public void getMaxSpeedPerKm(int maxSpeed) {
		  this.maxSpeed = maxSpeed;
	  };
	  // 
	  public void honk() {
	    System.out.println("Tu, tu!");
	  }
	}
